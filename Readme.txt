Curso de Udemy "Crea una página web profesional con HTML CSS y Javascript" por Erick Mines
Duración: 3 horas 

Herramientas:
Para que se descarguen iconos a ocupar: https://fontawesome.com/

Diseños en Photohop(PSD) para Bootstrap 4:
- Por Hackerthemes: https://hackerthemes.com/blog/bootstrap-4-psd-template/

Plantillas para Bootstrap 4:
- Admin Dashboard: https://www.bootstrapzero.com/bootstrap-template/bootstrap-4-admin-dashboard
- Modular Admin: https://github.com/modularcode/modular-admin-html

Generadores de Layout online para Bootstrap 4:
-.Pingendo: http://v4.pingendo.com/playground.html
- Bootply: http://www.bootply.com/
- Layoutit: http://www.layoutit.com/es
- Bootstrapstudio: https://bootstrapstudio.io/(Muy buena, pero de pago).

Novedades de Bootstrap 4.:

1. Soporte para flexbox: flexbox es una nueva forma de crear diseños usando CSS3 que nos permiten lograr características o comportamientos a los elementos que antes sno los conseguiamos de forma sencilla. Ahora podemos hacer que se alineen de forma horziontal, vertical, que tengan comportamientos fluidos, reordenar los elementos de forma visual sin tocar código fuente (la forma en que estan escritas los bloques del sitio).
2. Abandono de soporte para Internet Explorer 8.
3. Nueva unidad de medida (rems) para la tipografía. Estos permiten poder escalar el tamaño no solamente del texto, sino de diferentes elementos que conforman nuestra página. Esto nos ayuda cuando creamos sitios responsive. Un rem equivale al tamaño de fuente que se ha definido en la etiqueta html. Si digo que sea de 15 px, entonces 1 rem = 15px; 2 rem = 30px. Lo mismo para el padding
html{
	font-size: 15px;
}
h1{
	font-size: 2rem; /*30px*/
	padding: 1rem; /*15px*/
}
4. Nuevas y útiles clases
<p class="text-xs-left">Texto a la izquierda en una resolución</p>
<p class="text-md-right">Texto a la derecha en otra resolución</p>
5. Adiós Glyphicons
6. Nuevos componentes
7. Adiós Less, hola Sass ya que es un preprocesador que compila más rápido y es más fácil de usar
8. 30% más ligera
9. Mejor documentación